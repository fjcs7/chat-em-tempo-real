Rails.application.routes.draw do
  get 'shelfs', controller:'shelfs', action:'show'
  root to: 'chats#show'

  mount ActionCable.server => '/cable'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
